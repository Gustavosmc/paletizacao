/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paletizacaov10;

/**
 *
 * @author Adriane
 */
public class Caixa {

    private int x;
    private int y;
    private int orientacao;

    public Caixa(int x, int y, int orientacao) {
        this.x = x;
        this.y = y;
        this.orientacao = orientacao;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getOrientacao() {
        return orientacao;
    }

    public void setOrientacao(int orientacao) {
        this.orientacao = orientacao;
    }
}
